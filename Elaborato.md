**Elaborato esame di stato A.S. 2020-2021**

**Leonardo Canello, 5ᵃIA**

# Indice

- [Introduzione](#Introduzione)

- [Proxy](#proxy):
    - [Impieghi](#impieghi)
    - [Reverse proxy](#reverse-proxy)
        - [Load balancing](#load-balancing)
    - [Cache](#cache)
    - [Compressione](#compressione)
    - [Sicurezza](#sicurezza)
        - [Autenticazione](#autenticazione)
        - [Filtraggio](#filtraggio)
        - [Tipologie di filtraggio](#tipologie-di-filtraggio)
        - [Logging](#logging)
    - [Squid e NGINX](#squid-e-nginx)
    - [Tor](#tor)
<br>

- [MySQL](#mysql)
    - [Gestione dei permessi](#gestione-dei-permessi)
        - [Autorizzazioni per utente](#autorizzazioni-per-utente)
        - [Autorizzazioni per tabella](#autorizzazioni-per-tabella)
        - [Diverti tipi di permessi](#diversi-tipi-di-permessi)
    - [Risorse](#risorse)
        - [Imporre i limiti](#imporre-i-limiti)
        - [Utilizzi popolari](#utilizzi-popolari)
<br>

- [Un esempio nella realtà](#un-esempio-nella-realtà)
- [Una visione centralizzata](#una-visione-centralizzata)

- [Conclusione](#conclusione)

- [Presentazione](#presentazione)

- [Sitografia](#sitografia)

# Introduzione
In questo documento approfondirò 2 argomenti che all'apparenza sono lontani ma in realtà possono dipendere uno dall'altro. Verranno visti vari punti per entrambi i temi, per poi affrontare una ipotesi di una situazione reale in cui verrebbero usati. Mostrerò rappresentazioni delle realtà descritte e cercherò di rendere le informazioni il più chiaro possibile nonostante la tecnicità dell'argomento.

Tutti i contenuti dell'elaborato sono reperibili su gitlab a questo [link](https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse)


# Proxy
Il **proxy** è un tipo di server che fa da intermediario per le richieste da parte dei client alla ricerca di risorse su altri server.

## Impieghi
I server proxy vengono utilizzati per svariati impieghi come: 
- Fornire l'anonimato durante la navigazione internet (es. TOR)
- Memorizzare una copia delle richieste per poter fornire la risposta a richieste uguali senza dover effettuare nuovi accessi
- Creare una difesa (Firewall) verso il web, filtrando le connessioni in entrata, in uscita e controllando/modificando il traffico interno

## Reverse proxy
Il **reverse proxy** ha il conmpito di rendere disponibile un servizio in una rete interna ad una rete esterna tipo internet. Svolge varie funzioni inclusa la crittografia TLS, caching, compressione, load balancing ed autenticazione.

## Load balancing
Il reverse proxy può distribuire il carico su svariati web server, ognuno con il proprio applicativo. Questo permette una maggiore scalabilità dei servizi ed evita che il servizio vada offline in caso uno dei server abbia un malfunzionamento

Vediamo uno schema che rappresenta una rete in cui è presente un load balancer

<img alt="Webservers con davanti un loadbalancer" style="" width="50%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/InformaticaLoadBalancer.png" />

Come si può vedere la richiesta arriva da internet al reverse proxy che fa da load balancer. Esso suddividerà le richieste tra i 3 webserver all'interno della lan
i quali hanno accesso allo stesso database e sono configurati allo stesso modo.

## Cache
Serve ad alleggerire il lavoro del server salvandosi le risposte alle richieste degli utenti e fornendole istantaneamente senza fare nuove richieste al server. Questo permette una maggiore velocità del sito e meno carico sul web server

## Compressione
Il proxy può ottimizzare e comprimere i contenuti per velocizzare i tempi di caricamento del servizio

## Sicurezza
Il proxy può fornire uno strato aggiuntivo di difesa dagli attaccanti, in particolare se ha anche funzionalità di **firewall**

## Autenticazione
Il proxy offre un servizio di autenticazione HTTP dove gli utenti devono accedere per poter accedere al web. Si può quindi ricondurre l'utilizzo della rete ai singoli individui e monitorare l'utilizzo di banda

## Filtraggio
I proxy dediti al filtraggio web non riescono a controllare i dati all'interno di transazioni HTTP sicure almeno che la catena di fiducia di SSL/TLS non sia stata manomessa. In ambienti di lavoro è possibile però rendere nota la chiave privata al proxy in modo tale che possa analizzare il contenuto delle connessioni. 
Questo tipo di proxy spesso supportano l'autenticazione degli utenti e permettono una limitazione a siti e servizi on-line censurandoli. Questo strumento è utilizzato da scuole, aziende fino agli stati. 

## Tipologie di filtraggio
Per filtrare le richieste fatte dagli utenti si possono usare svariati metodi: 
- blacklist di URL 
- blacklist di DNS
- regex degli URL 
- parole chiave

## Logging
Questa funzionalità raccoglie i dati riguardo l'url da visitare, larghezza di banda utilizzata e con che ip un utente ha mandato richieste al proxy. È particolarmente utilizzato dagli amministratori di rete che devono ripsettare delle necessità legislative che impongono la conservazone di questi dati per un periodo che varia da stato a stato.

## Squid e NGINX
<img alt="Logo Squid" style="" width="5%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Squid-cache.png" />
**Squid** è un popolare software libero con funzionalità proxy, reverse proxy e cache. Supporta vari protocolli ma è utilizzato primariamente per HTTP e FTP. È compatibile con la maggior parte dei sistemi operativi.
<br/>
<img alt="Logo NGINX" style="" width="10%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Nginx_logo.svg.png" />
Un altro software dalle funzionalità simili è **NGINX**, nato come server web è diventato molto velocemente uno degli strumenti più utilizzati nell'industria grazie alle sue alte prestazioni e funzionalità.

## Tor
<img alt="Logo Tor" style="" width="5%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/tor-logo.png" /> Acronimo di **The Onion Router** è un software libero che permette una comunicazione anonima per internet basata sul protocollo onion. Tramite il suo utilizzo è molto difficile tracciare l'attività internet degli utenti proteggendo la loro privacy e libertà. Nasce per proteggere i servizi segreti statunitensi, è ad oggi lo strumento più utilizzato da chi vuole utilizzare internet in reale libertà o per scopi illegali.
Nel 2017 Filippo Cavallarin, un hacker veneziano, riuscì ad individuare una falla nel sistema Tor che permetteva di inviduare l'ip da cui iniziava la conenssione. Il bug fu prontamente risolto grazie ad una segnalazione da parte dell'hacker che decise di non trarne profitto.


# MySQL
<img alt="Logo Tor" style="" width="5%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/MySQL-Logo.png" /> È un RDBMS (Relational Database Management System) sviluppato per essere il più possibile agli standard ANSI (American National Standards Institute) e ODBC (Open Database Connectivity, un'api per accedere ai DBMS) per l'SQL.
È incorporato in piattaforme come **LAMP**, WAMP e **XAMPP** permettendo la creazione di siti web dinamici.
Esiste anche un fork (sono partiti dal codice di MySQL per crearne una loro versione) di MySQL che ha acquisito popolarità chiamato MariaDB anche grazie alla sua miglior interazione con la comunità e supporto a più piattaforme.

## Gestione dei permessi
MySQL permette di creare account che permetttono agli utenti di connettersi al server ed accedere ai dati gestiti da esso. Una delle funzioni primarie dei privilegi in MySQL è quella di autenticare l'utente che si connette ed associarlo ai permessi che ha su ogni database e permettere solo tali operazioni.

## Autorizzazioni per utente
Per aggiungere un permesso ad un utente bisogna prima crearlo:
```mysql
CREATE USER 'david'@'198.51.100.0/255.255.255.0';
```
Questo permetterà all'utente david di entrare solo se la seguente condizione è vera:Un esempio nella realtà
```
client_ip & netmask = host_ip
```
in questo caso l'utente potrà connettersi solo se l'ip è incluso tra `192.51.100.0` e `192.51.100.255`

## Autorizzazioni per tabella
Per ogni utente si può impostare permessi diversi su ogni tabella.
Il tutto è gestito con delle tabelle che hanno come dati: i privilegi dell'utente, il nome della tabella su cui hanno questi permessi, il database, le impostazioni di sicurezza dell'utente, e la gesitone delle risorse

## Diversi tipi di permessi
I permessi possibili sono:

- ALL - Autorizza l'accesso ad un intero database. Se non viene specificato un database il permesso viene applicato su tutto MySQL
- CREATE - Autorizza alla creazione di database e tabelle
- DELETE - autorizza la rimozione di tuple dalle tabelle
- DROP - autorizza la rimozione di tabelle e database
- EXECUTE - autorizza l'esecuzione di routine salvate
- GRANT OPTION - permette di togliere o aggiungere permessi ad un altro utente
- INSERT - permette l'inserimento di tuple in una tabella
- SELECT - permette all'utente la visione e selezioni di dati da un database
- SHOW DATABASES- autorizza la visione di una lista di tutti i database
- UPDATE - autorizza l'utente ad aggiornare una tupla in una tabella

Si può inoltre impostare questi permessi per l'intero database.

## Risorse
Quando si utilizza MySQL inevitabilmente si occuperanno delle risorse sul server, ovvero, il carico di lavoro per il server incrementa.
Quando l'accesso al database è limitato e il quantitativo di dati ridotto questo fenomeno è impossibile da rilevare mentre diventa importantissimo gestirlo in situazioni come quelle delle API (Application Programming Interface) dove la quantità di richieste può arrivare alle migliaia al secondo e si elabora una mole di dati molto più ampia. Se non si impongono limiti al numero di richieste e numero di connessioni si può quindi arrivare ad un rallentamento del server perchè sovraccaricato o un DOS (Denial Of Service) dove il servizio diventa irraggiungibile.

## Imporre i limiti
Grazie a MySQL possiamo imporre vari limiti:
- MAX_QUERIES_PER_HOUR - Impone un numero massimo di query per ora
- MAX_UPDATES_PER_HOUR - Impone un numero massimo di update per ora
- MAX_CONNECTIONS_PER_HOUR - Impone un numero massimo di connessioni per ora
- MAX_USER_CONNECTIONS - Impone un numero massimo di connessioni contemporanee per utente
Andiamo quindi a creare un utente con questi permessi:
```mysql
CREATE USER 'david'@'localhost' IDENTIFIED BY 'david'
    ->     WITH MAX_QUERIES_PER_HOUR 20
    ->          MAX_UPDATES_PER_HOUR 10
    ->          MAX_CONNECTIONS_PER_HOUR 5
    ->          MAX_USER_CONNECTIONS 2;
```
L'utente david potrà quindi fare un massimo di 20 query ogni ora. 

## Utilizzi popolari
Come detto precedentemente è comune usare questo tipo di restrizioni sulle API.
Alcuni servizi di questo tipo possono essere per esempio quelli che forniscono mappe nei siti per mostrare dove sia locato un luogo (OpenStreetMap ecc...). Sono infatti limitati ad un numero di richieste all'ora in modo tale che i server non vengano sovraccaricati. Si può però pagare per avere un limite più elevato in caso se ne abbia la necessità.

# Un esempio nella realtà
Conoscendo queste funzionalità di questi 2 servizi è possibile ideare un esempio di utilizzo nella vita reale che  sia sufficientemente comune.

Si inserisce un proxy in una rete scolastica che filtra le connessioni al livello applicativo HTTP. Tutte le richieste che arrivano dalla rete interna sono suddivise in varie vlan, reti virtuali che identificano delle stanze nell'edificio. Come esempio prenderemo la vlan "segreteria" la quale ha come subnet 192.168.1.0/24. Quando il proxy riceve la richiesta di connessione a qualche sito, mettiamo caso facebook, richiede all'utente di accedere per poter utilizzare la rete. L'utente accede ma la pagina web non viene comunque caricata, viene invece rimpiazzata da un avviso dove viene segnalato che il proxy ha bloccato quella richiesta.
Questo comportamente è dovuto ad una regola impostata dall'amministratore di rete che vieta l'accesso a facebook e tutti i suoi sottodomini (es: api.facebook.com).
Sempre la segreteria ha bisogno di accedere ad un database dove vengono salvate tutte le informazioni di cui hanno bisogno. Nello stesso database sono però presenti anche dati sensibili come ad esempio se uno studente è DSA oppure informazioni sugli stipendi dei docenti.
L'amministratore ha quindi impostato agli utenti della segreteria,  come ad esempio Mario Rossi, la sola possibilità di leggere le tabelle in cui sono presenti i dati ai quali possono accedere e la possibilità di inserire e modificare in quelle dove tale azione è necessaria al lavoro degli stessi.

Questo tipo di infrastruttura si può notare possa tranquillamente essere applicata alla nostra rete scolastica e probabilmente già lo è, almeno in parte.

Si può anche scegliere di aggiungere una restrizione in più sugli accessi al database: si può filtrare l'ip da cui arriva la richiesta di accesso. Mettiamo caso che Mario Rossi stia cercando di accedere dalla segreteria con l'ip 192.168.1.54. Il server sa che questa subnet è autorizzata e lascia accedere l'utente. Se però queste credenziali vengono rubate da un malfattore esso può abusare di esse per accedere e modificare dei dati. Il malfattore accede da un'altra area della scuola con l'utente Mario Rossi e l'ip 192.168.2.43. La subnet corrispondente all'ip è differente da quella autorizzata dal server e quindi non permetterà l'autenticazione.

<img alt="Rappresentazione grafica del blocco" style="" width="50%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/InformaticaAccessoDBRete.png" />

Se per esempio volessimo garantire solo l'operazione di lettura sulla tabella 'segreteria' all'utente 'mrossi' nella subnet 192.168.1.0 dovremo:
1) Creare l'utente con la sola possibilità di accesso dalla subnet voluta e con la password 'password123':
```mysql
CREATE USER 'mrossi'@'192.168.1.%' IDENTIFIED BY 'password123';
```
2) Aggiungiamo il permesso di lettura all'utente sulla tabella segreteria
```mysql
GRANT SELECT ON scuola.segreteria TO 'mrossi'@'192.168.1.%';
```
3) Controlliamo i permessi dell'utente grazie al comando SHOW:
```mysql
SHOW GRANTS FOR 'mrossi'@'192.168.1.%';
```
<img alt="Rappresentazione grafica degli accessi" style="" width="50%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/InformaticaAccessoDBSelect.png" />

Ora che Mario Rossi ha accesso al database con le informazioni che gli servono
dobbiamo preoccuparci di assicurargli una connessione ad internet per poter utilizzare i servizi web di cui necessita. Vogliamo però poter risalire a quale computer utilizzerà in segreteria l'utente ed evitare l'accesso ad eventuali siti web inappropriati  all'ufficio. Introduciamo allora NGINX un software che permette l'autenticazione tramite HTTP. Essendo tutto il traffico indirizzato al proxy si avrà 
che il proxy prima di permettere l'accesso ad internet richiederà l'autenticazione tramite username e password. Lo schema per il funzionamento dell'autenticazione potrebbe somigliare a quello in immagine

<img alt="Rappresentazione grafica dell'accesso tramite proxy" style="" width="50%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/InformaticaAccessoInternetProxy.png" />

Analizziamo i vari punti:
1) Il client fa la richiesta al proxy per visitare un sito
2) Il proxy richiede al database se quell'utente ha l'autorizzazione ad usare internet. Notare come il proxy in questo caso abbia accesso alla tabella utenti al contrario di mrossi
3) Il database ritorna 1 se l'utente ha l'accesso oppure 0 se non lo ha
4) Se il proxy riceve 1 come risposta inoltra la richiesta al sito di destinazione permettendo la navigazione

Una cosa simile si può immaginare per il blocco dei siti dove il proxy va a controllare il sito non sia nella blacklist.

# Una visione centralizzata

Nell'esempio appena visto i servizi sono isolati ad una rete di dimensioni ridotte e con un'utenza non molto elevata. 
Cosa succederebbe però se il servizio da gestire fosse a livello statale con un database centrale che ad esempio contiene i vaccini fatti da ogni singola persona?
Si otterrà ovviamente un quantitativo di dati molto elevato ed un numero di richieste possibilmente elevato. 
Dovremo quindi creare utenti con delle restrizioni che permettano un maggior controllo sull'utilizzo delle risorse.
1) Creiamo l'utente dedicato alla signora Pina Fantozzi, adibita alla registrazione delle vacchinazioni:
```mysql
CREATE USER 'pfantozzi'@'%' IDENTIFIED BY 'password123'
    ->     WITH MAX_QUERIES_PER_HOUR 30
    ->          MAX_UPDATES_PER_HOUR 10
    ->          MAX_CONNECTIONS_PER_HOUR 5
    ->          MAX_USER_CONNECTIONS 2;
```
Se analizziamo la query noteremo come la connessione sia autorizzata per qualunque ip grazie al simbolo % permettendo a Pina di accedere da qualunque rete con accesso ad internet. I vari parametri sono scelti arbitrariamente dall'amministratore di sistema il quale ritiene siano sufficienti 30 query per ora perchè è improbabile una visita/vaccino richieda meno di 2 minuti.

2) Aggiungiamo i permessi sulla tabella all'utente:
```mysql
GRANT SELECT, INSERT, UPDATE ON vaccini.persone TO 'pfantozzi'@'%';
```
In questo caso l'utente può vedere i dati nella tabella persone del database vaccini, può aggiornare i dati e può inserirne di nuovi.

Il proxy che si frappone fra Pina Fantozzi e il server MySQL invece non subirà variazioni rispetto a quello utilizzato nell'ambito scolastico.
La funzione del proxy sarà sempre la stessa, cambieranno solo le tabelle presenti all'interno del database tranne che la tabella utente e blacklist, ma questo all'utente non fa differenza.

Più interessante è invece vedere il funzionamento del reverse proxy che rende possibile l'accesso al database.
<img alt="Rappresentazione grafica dell'accesso tramite reverse proxy" style="" width="70%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/InformaticaAccessoMySQLReverseProxy.png" />
1) La richiesta arriva al firewall che consente di avere una maggior sicurezza della rete
2) Le richieste che passano il firewall arrivano al reverse proxy il quale rende disponibili vari servizi nella lan interna, incluso il server MySQL.
Questo cosa significa? Che invece di avere tutti i server che necessitano di essere accessibili da internet esposti li si nasconde dietro il reverse proxy. Il reverse proxy poi sceglierà a chi indirizzare la richiesta ricevuta.
Prendiamo per esempio il domino sanita.it. Questo dominio corrisponderà ad un sito ospitato su un webserver all'interno della lan dove è presente anche il nostro server MySQL. Quando il reverse proxy riceve una richiesta per sanita.it inoltrerà quindi la richiesta al webserver nella lan interna il quale offrirà il proprio servizio. 
3) Possiamo fare la stessa cosa con il server MySQL e per esempio renderlo accessibile al sottodominio mysql.sanita.it.

Avere il reverse proxy a smistare le richieste permetterà inoltre un'alleggerimento del carico di richieste ai server ed in caso ci fosse un server che va offline lo si può facilmente rimpiazzare con un'altro di backup nel mentre che lo si sistema.
Aziende che devono ospitare grandi carichi di utenti attuano il load balancing ovvero una suddivisione dell'utenza su più server in modo tale che non vengano mai sovraccaricati.


# Conclusione
Oggi più che mai una buona capacità nel gestire le infrastrutture che supportano i servizi che utilizziamo ogni giorno è di vitale importanza e gli strumenti di cui ho parlato sono fondamentali per raggiungere questo obiettivo. 
Ritengo sia importante sensibilizzare gli studenti su queste tematiche in modo tale che quando affronteranno l'ambiente lavorativo sappiano fare le giuste considerazioni quando progettano. La conoscenza teorica ovviamente differisce in parte dall'attuazione pratica ma, con un po' di pazienza, permette di avere conoscenze utili quando si vuole provare ad utilizzare questi strumenti nella realtà. 
Questo tipo di conoscenze possono evitare situazioni disastrose come i famosi "clickday" dove migliaia di persone accedono allo stesso servizio in poco tempo rendendolo inagibile per ore se non giorni, oppure assicurare che un servizio sia sempre online nel settore ospedaliero possibilmente salvando vite.


# Presentazione
<img alt="Prima slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti.png" />
<img alt="Seconda slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(1).png" />
<img alt="Terza slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(2).png" />
<img alt="Quarta slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(3).png" />
<img alt="Quinta slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(4).png" />
<img alt="Sesta slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(5).png" />
<img alt="Settima slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(6).png" />
<img alt="Ottava slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(7).png" />
<img alt="Nona slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(8).png" />
<img alt="Decima slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(9).png" />
<img alt="Undicesima slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(10).png" />
<img alt="Dodicesima slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(11).png" />
<img alt="Tredicesima slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(12).png" />
<img alt="Quattordicesima slide" style="" width="100%" src="https://gitlab.com/_Zaizen_/maturit-proxy-e-mysql-permessi-e-risorse/-/raw/master/Proxy%20e%20MySQL,%20un'avventura%20come%20sistemisti%20(13).png" />


# Sitografia
https://it.wikipedia.org/
https://www.mysql.com/
https://arxiv.org/pdf/1302.4046.pdf
https://en.wikipedia.org
https://www.nginx.com/
https://www.apachefriends.org/
